package source;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
	// write your code here
    Proceso p1 = new Proceso();
    Proceso p2 = new Proceso();
    Proceso p3 = new Proceso();
    Proceso p4 = new Proceso();
    Proceso p5 = new Proceso();
    p1.setId(5); p1.setLlegada(20);
    p2.setId(10); p2.setLlegada(2);
    p3.setId(4); p3.setLlegada(1);
    p4.setId(6); p4.setLlegada(4);
    p5.setId(2); p5.setLlegada(8);

    ArrayList<Proceso> procesos_1 = new ArrayList<>();
    ArrayList<Proceso> procesos_2 = new ArrayList<>();

    procesos_1.add(p1); procesos_2.add(p1);
    procesos_1.add(p2); procesos_2.add(p2);
    procesos_1.add(p3); procesos_2.add(p3);
    procesos_1.add(p4); procesos_2.add(p4);
    procesos_1.add(p5); procesos_2.add(p5);

    Collections.sort(procesos_1);
    Collections.sort(procesos_2, new Comparar_X_Llegada());

    System.out.println(procesos_1);
    System.out.println(procesos_2);

    }
}
