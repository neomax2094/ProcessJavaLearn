package source;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Comparar_X_Llegada implements Comparator{
    @Override
    public int compare(Object o1, Object o2) {
        if (o1==null || o2==null){
            return -1;
        }
        Proceso p1=(Proceso)o1;
        Proceso p2=(Proceso)o2;
        if(p1.getLlegada()<p2.getLlegada()){
            return -1;
        }else if(p1.getLlegada()>p2.getLlegada()){
            return 1;
        }else {
            return 0;
        }
    }

}
