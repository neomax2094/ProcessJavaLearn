package source;

import java.util.Objects;

public class Proceso implements Comparable{
    private int id;
    private int llegada;
    private int duracion;
    private int prioridad;
    private int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLlegada() {
        return llegada;
    }

    public void setLlegada(int llegada) {
        this.llegada = llegada;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proceso proceso = (Proceso) o;
        return id == proceso.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Proceso{" +
                "id=" + id +
                ", llegada=" + llegada +
                ", duracion=" + duracion +
                ", prioridad=" + prioridad +
                ", status=" + status +
                "}\n";
    }

    @Override
    public int compareTo(Object o) {
        if(o==null){
            return -1;
        }if(!(o instanceof Proceso)){
            return -1;
        }
        Proceso p=(Proceso)o;
        Integer i = id;
        return i.compareTo(p.getId());
    }
}
